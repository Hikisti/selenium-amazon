# selenium-amazon

This is a repository for Selenium Web browser automated tests with Java language.

These tests use Chrome and Firefox browsers.

Test opens Amazon web site, then it uses search for looking Nikon. After that it sorts product list from highest price
to lowest.
Then it opens the second product of the list and verifies that product title contains text Nikon D5.

Java 8 JDK and Maven must be installed. Also JAVA_HOME environment variable must be set. Chrome and Firefox must be
installed.

You can run test with Maven, use command:

mvn clean test
