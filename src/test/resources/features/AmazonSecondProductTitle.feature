Feature: Product title contains text Nikon D5
This feature verifies that second product title contains text Nikon D5 when searching Nikon products with price
high to low filter

Scenario: Check that product title contains text Nikon D5 when running Chrome browser

Given User launches Chrome browser
When User runs "https://www.amazon.com/" homepage
Then User types Nikon in search box
And User sort results from highest prices to lowest
And User selects second product after filtering
And User verifies that product title contains text "Nikon D5"

Scenario: Check that product title contains text Nikon D5 when running Firefox browser

Given User launches Firefox browser
When User runs "https://www.amazon.com/" homepage
Then User types Nikon in search box
And User sort results from highest prices to lowest
And User selects second product after filtering
And User verifies that product title contains text "Nikon D5"
