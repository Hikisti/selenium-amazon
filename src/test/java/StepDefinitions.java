package test.java;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class StepDefinitions {

    public WebDriver driver;
    WebDriverWait wait;
    public static final String EXPECTED_BASE_TITLE = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";

    @Given("^User launches Chrome browser$")
    public void user_launches_Chrome_browser() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
    }

    @When("^User runs \"([^\"]*)\" homepage$")
    public void user_runs_homepage(String arg1) {
        driver.get(arg1);

        if (!wait.until(ExpectedConditions.titleContains(EXPECTED_BASE_TITLE)) || !wait.until(ExpectedConditions.urlContains(arg1))) {
            throw new RuntimeException("Amazon main page is not displayed");
        }
    }

    @Then("^User types Nikon in search box$")
    public void user_types_Nikon_in_search_box() {
        WebElement searchBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(("#twotabsearchtextbox"))));
        searchBox.sendKeys("Nikon");

        WebElement searchButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector((".nav-search-submit"))));
        searchButton.click();
    }

    @Then("^User sort results from highest prices to lowest$")
    public void user_sort_results_from_highest_prices_to_lowest() {
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sort")));
        element.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("option")));
        List<WebElement> menuItems = element.findElements(By.tagName("option"));
        for(WebElement menuItem : menuItems) {
            if (menuItem.getText().equals("Price: High to Low")) {
                menuItem.click();
            }
        }
    }

    @Then("^User selects second product after filtering$")
    public void user_selects_second_product_after_filtering() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/descendant::div[@class='a-row a-spacing-none scx-truncate-medium sx-line-clamp-2'][2]")));
        WebElement secondProductInSortedList = driver.findElement(By.xpath("/descendant::div[@class='a-row a-spacing-none scx-truncate-medium sx-line-clamp-2'][2]"));
        secondProductInSortedList.click();
    }

    @Then("^User verifies that product title contains text \"([^\"]*)\"$")
    public void user_verifies_that_product_title_contains_text(String arg1) {
        wait.until(ExpectedConditions.titleContains(arg1));
        String titleOfSecondProduct = driver.getTitle();

        Assert.assertTrue(titleOfSecondProduct.contains(arg1));

        driver.quit();
    }

    @Given("^User launches Firefox browser$")
    public void user_launches_Firefox_browser() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
    }
}
